<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="x-ua-compatible" content="ie=edge" />
  <title><?php print $page_title; ?></title>
  <?php print $styles; ?>
  <noscript>
    <link rel="stylesheet" href="/<?php print $directory; ?>/assets/css/mobile.min.css" />
  </noscript>
  <script>
    var ADAPT_CONFIG = {
      path: '/<?php print $directory; ?>/assets/css/',
      dynamic: true,
      range: [
        '760px            = mobile.min.css',
        '760px  to 980px  = 720.min.css',
        '980px  to 1280px = 960.min.css',
        '1280px to 1600px = 1200.min.css',
        '1600px to 1920px = 1560.min.css',
        '1920px           = fluid.min.css'
      ]
    };
  </script>
  <script src="/<?php print $directory; ?>/assets/js/adapt.min.js"></script>
</head>
<body>
<div class="container_12 clearfix">
  <?php print $content; ?>
</div>
<?php print $scripts; ?>
<?php print $closure; ?>
</body>
</html>